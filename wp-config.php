<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'edyl' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7cRm7&6xdnP4o)EYuY8y [f4eZm)pf+X:^FzWcy7rq[gU|`/zVK@/7*2Qn04?,CW' );
define( 'SECURE_AUTH_KEY',  '(XJZXW!5@=RU628BvDNk8jgoZxJ6A_S:kH5OBV}K%}|G+qP+V]igCS<VLHYXs[cV' );
define( 'LOGGED_IN_KEY',    'u ^{6tBZPamz5j?_(DPQ+sD%k>l_vn`s#;zo HjL7clNuCK=}0,^i|;/KUzR9qsm' );
define( 'NONCE_KEY',        'R,/[B?ly9Ijc3ipEP WWc4@be627n{w&HW IF1k7hpzCjnKeOeL0iyv%9Itb4k3o' );
define( 'AUTH_SALT',        '/lW%CMsI|NhwQ[F@ENnYOw&4:$kL=zO5hfqY&y:a3`qMyp>tvhW64&1(=.hUlj`r' );
define( 'SECURE_AUTH_SALT', 'm7HI^1=+lY~aI,]p,s!)Nh&QhSG`%W nihlTauEye&P3!V+ *(e?K;>XGgO#76j:' );
define( 'LOGGED_IN_SALT',   'sj?#,eIQ2sW09|;2BS7#:?%!uOyzN4g1tDLXIsaJ8TOq~:=wJCy67{&gy(>naB=b' );
define( 'NONCE_SALT',       'z9-A@Lh4he`d77fR/5Rrj~.EN_HgVvF-I;zxV3o1CpzgK_&kN<b5kN>P,}~{B(@A' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
