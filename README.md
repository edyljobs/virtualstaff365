===

Hi sir Yuri this is my hands on test for virtualstaff365.

Thanks ,
Edyl

Installation
---------------
1.  clone this git repo
2.  when cloned instead of virtualstaff365-master remove -master from the folder name so it should be virtualstaff365
3.  inside the cloned repo is an sql file called edyl.sql create a database name it edyl and import it there
4.  copy the clone repo and put it in htdocs , www or root of the local environment
5.  run local server and access the test site  localhost/virtualstaff365/

### Requirements 
xampp , wampp or other local environment

### Description
I used scss + BEM you can locate in theme file virtualstaff365/style.scss 
The autogenerated css file I already minified it using https://cssminifier.com/ and added some cache functionality for css , js and html for site speed using autooptimize.  